package xyz.yk20.blog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@ComponentScan("xyz.yk20.blog.po")//启动类默认只扫描注解的类的同包以及子包下的类，若不在同一个包下，需要指明扫描
//@EntityScan("po")
public class BlogApplication {

    public static void main(String[] args) {
        System.out.println("项目启动啦！");
        SpringApplication.run(BlogApplication.class, args);
    }

}
