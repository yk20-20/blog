package xyz.yk20.blog.service;

import xyz.yk20.blog.po.User;

public interface UserService {
    User checkUser(String username, String password);
}
