package xyz.yk20.blog.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.yk20.blog.dao.UserRepository;
import xyz.yk20.blog.po.User;
import xyz.yk20.blog.utils.MD5Utils;

/**
 * Create by yk20 on 2020/7/5 18:51
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User checkUser(String username, String password) {
        User user = userRepository.findByUsernameAndPassword(username, MD5Utils.code(password));
        return user;
    }
}
