package xyz.yk20.blog.service;

import xyz.yk20.blog.po.Comment;

import java.util.List;

/**
 * Create by yk20 on 2020/7/8 18:27
 */
public interface CommentService {

    List<Comment> listCommentByBlogId(Long blogId);

    Comment saveComment(Comment comment);
}
