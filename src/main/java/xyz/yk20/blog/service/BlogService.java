package xyz.yk20.blog.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import xyz.yk20.blog.po.Blog;
import xyz.yk20.blog.vo.BlogQuery;

import java.util.List;
import java.util.Map;

/**
 * Create by yk20 on 2020/7/6 21:28
 */
public interface BlogService {

    Blog getBlog(Long id);

    Blog getAndConvert(Long id);

    Page<Blog> listBlog(Pageable pageable, BlogQuery blog);

    Page<Blog> listBlog(Pageable pageable);

    Page<Blog> listBlog(Long tagId, Pageable pageable);

    Page<Blog> listBlog(String query, Pageable pageable);

    List<Blog> listRecommendBlogTop(Integer size);

    Map<String, List<Blog>> archiveBlog();

    Long countBlog();

    Blog saveBlog(Blog blog);

    Blog updateBlog(Long id, Blog blog);

    void deleteBlog(Long id);

    //小程序个人博客首页
    List<Map<String, Object>> listBlogPart();

    //小程序个人博客详情
    List<Map<String, Object>> listBlog(Long id);
}
