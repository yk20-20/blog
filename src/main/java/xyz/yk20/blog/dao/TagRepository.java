package xyz.yk20.blog.dao;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import xyz.yk20.blog.po.Tag;
import xyz.yk20.blog.po.Type;

import java.util.List;

/**
 * Create by yk20 on 2020/7/6 11:00
 */
public interface TagRepository extends JpaRepository<Tag, Long> {
    Tag findByName(String name);

    @Query("select t from Tag t")
    List<Tag> findTop(Pageable pageable);
}
