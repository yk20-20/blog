package xyz.yk20.blog.dao;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import xyz.yk20.blog.po.Comment;

import java.util.List;

/**
 * Create by yk20 on 2020/7/8 18:31
 */
public interface CommentRepository extends JpaRepository<Comment, Long> {
    List<Comment> findByBlogIdAndParentCommentNull(Long blogId, Sort sort);
}
