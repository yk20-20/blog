package xyz.yk20.blog.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import xyz.yk20.blog.po.User;

/**
 * Create by yk20 on 2020/7/5 18:55
 */
//对User类进行查询，Long为主键类型
public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsernameAndPassword(String username, String password);
}
