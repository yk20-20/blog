package xyz.yk20.blog.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import xyz.yk20.blog.po.Blog;

import java.util.List;
import java.util.Map;

/**
 * Create by yk20 on 2020/7/6 21:33
 */
public interface BlogRepository extends JpaRepository<Blog, Long>, JpaSpecificationExecutor<Blog> {

    @Query("select b from Blog b where b.recommend = true")
    List<Blog> findTop(Pageable pageable);

    //select * from Blog where title like '%内容%'
    @Query("select b from Blog b where b.title like ?1 or b.content like ?1")
    Page<Blog> findByQuery(String query, Pageable pageable);

    @Transactional
    @Modifying       //更新所需要的注解
    @Query("update Blog b set b.views = b.views + 1 where b.id = ?1")
    int updateViews(Long id);

    @Query("select function('date_format', b.updateTime, '%Y') as year from Blog b group by function('date_format', b.updateTime, '%Y') order by 'year' desc")
    List<String> findGroupYear();

    @Query("select b from Blog b where function('date_format', b.updateTime, '%Y') = ?1")
    List<Blog> findByYear(String year);

//    @Query("select b.firstPicture,b.title,b.updateTime,b.user.nickname,b.user.avatar,b.views,b.type,b.description from Blog b")
//    List<Blog> findSome();
//
//    @Query("select b.firstPicture,b.title,b.updateTime,b.views,b.type,b.description from Blog b where b.id = ?1")
//    List<Blog> findBlogById(Long id);

    @Query(value = "SELECT b.id,b.first_picture,b.title,b.update_time,u.nickname,u.avatar,b.views,t.name type,b.description"+" FROM t_blog b,t_user u,t_type t WHERE b.user_id=u.id AND b.type_id=t.id",nativeQuery = true)
    List<Map<String, Object>> findSome();

    @Query(value = "SELECT b.id,b.first_picture,b.content,b.flag,b.title,b.update_time,u.nickname,u.avatar,b.views,t.name type,b.description"+" FROM t_blog b,t_user u,t_type t WHERE b.id=?1 AND b.user_id=u.id AND b.type_id=t.id",nativeQuery = true)
    List<Map<String, Object>> findBlog(Long id);
}
