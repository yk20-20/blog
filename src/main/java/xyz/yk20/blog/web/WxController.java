package xyz.yk20.blog.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import xyz.yk20.blog.po.Blog;
import xyz.yk20.blog.service.BlogService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Create by yk20 on 2020/7/16 18:47
 */
@RestController
public class WxController {
    @Autowired
    BlogService blogService;

    @GetMapping("/wx")
    public List<Map<String, Object>> index(){
        List<Map<String, Object>> blogs = blogService.listBlogPart();
        return blogs;
    }

    @GetMapping("/wx/blog")
    public List<Map<String, Object>> blog(@RequestParam("id") Long id){
        List<Map<String, Object>> blogs = blogService.listBlog(id);
        return blogs;
    }
}
