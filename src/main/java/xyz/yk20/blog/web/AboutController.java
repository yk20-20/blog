package xyz.yk20.blog.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import xyz.yk20.blog.service.BlogService;

/**
 * Create by yk20 on 2020/7/8 12:02
 */
@Controller
public class AboutController {

    @Autowired
    private BlogService blogService;

    @GetMapping("/about")
    public String about(Model model){
        model.addAttribute("recommendBlogsFooter",blogService.listRecommendBlogTop(3));
        return "about";
    }
}
