package xyz.yk20.blog.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import xyz.yk20.blog.service.BlogService;
import xyz.yk20.blog.service.TagService;
import xyz.yk20.blog.service.TypeService;

/**
 * Created by yk20 on 2020.7.4
 */

@Controller
public class IndexController {
    @Autowired
    private BlogService blogService;
    @Autowired
    private TypeService typeService;
    @Autowired
    private TagService tagService;

    @GetMapping("/")
    public String index(@PageableDefault(size = 6, sort = {"updateTime"}, direction = Sort.Direction.DESC) Pageable pageable,
                        Model model) {
        model.addAttribute("page", blogService.listBlog(pageable));
        model.addAttribute("types",typeService.listTypeTop(6));
        model.addAttribute("tags",tagService.listTagTop(10));
        model.addAttribute("recommendBlogs",blogService.listRecommendBlogTop(8));
        model.addAttribute("recommendBlogsFooter",blogService.listRecommendBlogTop(3));
        return "index";
    }
    //博客详情
    @GetMapping("/blog/{id}")
    public String blog(@PathVariable Long id, Model model){
        model.addAttribute("blog", blogService.getAndConvert(id));
        model.addAttribute("recommendBlogsFooter",blogService.listRecommendBlogTop(3));
        return "blog";
    }

    @PostMapping("/search")
    public String search(@PageableDefault(size = 6, sort = {"updateTime"}, direction = Sort.Direction.DESC) Pageable pageable,
                         @RequestParam String query, Model model){
        model.addAttribute("page",blogService.listBlog("%"+query+"%", pageable));
        model.addAttribute("recommendBlogsFooter",blogService.listRecommendBlogTop(3));
        model.addAttribute("query", query);
        return "search";
    }

    @GetMapping("/music")
    public String bgcar(Model model){
        model.addAttribute("recommendBlogsFooter",blogService.listRecommendBlogTop(3));
        return "music";
    }
}
